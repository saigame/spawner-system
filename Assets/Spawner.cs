using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public bool spawning = false;
    public List<Demon> demons;
    public List<WaveData> waves;
    public int waveIndex = 0;
    public float timer;

    private void Start()
    {
        Debug.Log("Game started");
        this.HideDemons();
        StartCoroutine(Spawning());
    }

    private void Reset()
    {
        this.LoadDemons();
        this.LoadWaves();
    }

    protected virtual void HideDemons()
    {
        foreach (Demon demon in this.demons)
        {
            demon.gameObject.SetActive(false);
        }
    }

    IEnumerator Spawning()
    {
        WaveData currentWave = this.waves[this.waveIndex];
        yield return new WaitForSeconds(currentWave.spawnDelay);
        StartCoroutine(this.SpawnWave(currentWave));
        this.waveIndex++;

        if (this.waveIndex < this.waves.Count) StartCoroutine(Spawning());
        else Debug.Log("All waves started");
    }


    IEnumerator SpawnWave(WaveData wave)
    {
        Debug.Log(wave.enemyName + ": " + wave.spawnMax);
        Demon demon = this.demons.Find((demo) => demo.name == wave.enemyName);
        GameObject newDemon = Instantiate(demon.gameObject);
        newDemon.transform.position = transform.position;
        newDemon.SetActive(true);
        Destroy(newDemon, 7);

        if (wave.spawnCount < wave.spawnMax-1)
        {
            wave.spawnCount++;
            yield return new WaitForSeconds(wave.spawnBetween);
            StartCoroutine(this.SpawnWave(wave));

        }
    }

    protected virtual void LoadDemons()
    {
        foreach (Transform child in transform)
        {
            Demon demon = child.GetComponent<Demon>();
            this.demons.Add(demon);
        }
    }

    protected virtual void LoadWaves()
    {
        this.waves.Add(this.Wave1());
        //this.waves.Add(this.Wave2());
        this.waves.Add(this.Wave3());
    }

    protected virtual WaveData Wave1()
    {
        WaveData waveData = new WaveData
        {
            spawnDelay = 2f,
            enemyName = DemonName.Demon.ToString(),
            spawnMax = 5,
            spawnBetween = 1f
        };
        return waveData;
    }

    protected virtual WaveData Wave2()
    {
        WaveData waveData = new WaveData
        {
            spawnDelay = 7f,
            enemyName = DemonName.Demon.ToString(),
            spawnMax = 9,
            spawnBetween = 1f
        };
        return waveData;
    }

    protected virtual WaveData Wave3()
    {
        WaveData waveData = new WaveData
        {
            spawnDelay = 9f,
            enemyName = DemonName.CaptrainDemon.ToString(),
            spawnMax = 1,
            spawnBetween = 1f
        };
        return waveData;
    }
}
