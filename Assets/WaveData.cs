using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WaveData
{
    public string enemyName;
    public float spawnDelay = 1;
    public int spawnCount = 0;
    public int spawnMax = 1;
    public float spawnBetween = 1;
}
